# USB UII LOGGER




![USB_UII_LOG](usb_uii_log.jpg)



[Schéma](usb_uii_logger.pdf) modulu USB_UII_LOGGER.

[PlatformIO firmware](https://gitlab.com/public903/firmware/usb_uii_log) pre Arduino NANO modul.

[Node-red flow](https://gitlab.com/public903/software/node-red/usb-uii-log.git) pre USB komunikáciu s modulom.



USB loger na meranie napätia a prúdu. Hardware pozostáva z dosky Arduino Nano, modul 16-bit ADC prevodníka ADS1115, a dva izolované prúdové snímače ACS712-20. 



**Špecifikácia:**

- Napäťový rozsah: 0-20V

- prúdový rozsah: -20 - +20 A

**Protokol**

9600 8 n 1

U?\n	-	získať napätie

I1?\n	-	získať prúd 1

I2?\n	-	získať prúd 2

